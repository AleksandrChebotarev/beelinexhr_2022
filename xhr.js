const auth = () => {
    const xhr = new XMLHttpRequest();
  
    xhr.open("POST", "https://reqres.in/api/login");
  
    xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
  
    xhr.send(
      JSON.stringify({
        username: "george.bluth@reqres.in",
        email: "george.bluth@reqres.in",
        password: "george",
      })
    );
  
    xhr.onreadystatechange = () => {
      console.log("State first request: ", xhr.readyState);
      if (xhr.readyState === 4) {
          if (xhr.status != 200) {
              alert(`Ошибка ${xhr.status}: ${xhr.statusText}`);
            } else {
              const tokenUser = JSON.parse(xhr.response).token;
              getUsersEmail(tokenUser);
            }
      }
    };
  };
  
  const getUsersEmail = (token) => {
    const xhr = new XMLHttpRequest();
  
    xhr.open("GET", "https://reqres.in/api/users?per_page=10");
  
    xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
    xhr.setRequestHeader("Accept-Language", "ru-RU, ru;q=0.9, en-US;q=0.8, en;q=0.7, fr;q=0.6");
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.setRequestHeader("Authorization", token);
  
    xhr.send();
  
    xhr.onreadystatechange = () => {
      console.log("State second request: ", xhr.readyState);
      if (xhr.readyState === 4) {
        if (xhr.status != 200) {
          alert(`Ошибка ${xhr.status}: ${xhr.statusText}`);
        } else {
          const emailUsers = JSON.parse(xhr.response).data.map((u) => u.email);
          console.log(emailUsers);
        }
      }
    };
  };
  
  document.addEventListener("DOMContentLoaded", auth);